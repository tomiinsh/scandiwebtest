<?php
	namespace App\Core;
	use App\Core\Database\DB;
	use App\Core\QueryBuilder\QueryBuilder;
	use ReflectionClass;
	use ReflectionProperty;

	abstract class BaseModel {

		/**
	     * The name of the table associated with the model.
	     *
	     * @var string
	     */		
		protected $table;

		/**
	     * The list of all fillable propeties of the class.
	     *
	     * @var array
	     */		
		protected $fillable;

		public function __construct()
		{
			$this->table = parse_class_name(get_class($this));
		}

		/**
	     * Saves object to database.
	     *
	     * @return void
	     */			
		public function save(): void
		{
			$this->newQueryBuilder()->insert($this->getPropertyArray());
		}

		/**
	     * Builds property array with fillable properties.
	     *
	     * @return array
	     */
		private function getPropertyArray(): array
		{
			$props = [];

			foreach(get_object_vars($this) as $key => $value)
			{
				if(in_array(strtolower($key), $this->fillable)) $props[strtolower($key)] = $value;			
			}

			return $props;		
		}

	    /**
	     * Returns all rows for the model.
	     *
	     * @return array
	     */
		public static function all(): array
		{
			return static::newQueryBuilder()->select()->get();
		}

	    /**
	     * Create new query builder.
	     *
	     * @return App\Core\QueryBuilder\QueryBuilder
	     */
	    public static function newQueryBuilder(): QueryBuilder
	    {
	    	return new QueryBuilder(parse_class_name(get_called_class()));
	    }
	}
<?php
	namespace App\Core\Validator;
	use App\Core\QueryBuilder\QueryBuilder;

	class Validator
	{
		/**
	     * Array of rules from user input.
	     *
	     * @var array
	     */			
		private $rules = [];

		/**
	     * Request that will be validated.
	     *
	     * @var array
	     */		
		private $request = [];

		/**
	     * List of all available rules.
	     *
	     * @var array
	     */			
		private $availableRules = ['required', 'exists', 'unique', 'length'];

		/**
	     * The state of the error.
	     *
	     * @var bool
	     */			
		private $error = false;

		/**
	     * Error message associated with the error.
	     *
	     * @var string
	     */				
		private $errorMessage = "";

	    /**
	     * Creates new Validator instance.
	     *
	     * @param $rules
	     * @return void
	     */	
		public function __construct($rules)
		{
			$this->rules = $rules;
		}

	    /**
	     * Validation of all rules.
	     *
	     * @param $request
	     * @return bool
	     */	
		public function validate($request): bool
		{
			$this->request = $request;

			foreach($this->rules as $key => $rule) 
			{
				$this->check($key, $rule);
				if($this->error == true) return false;
			}

			return !$this->error;
		}

	    /**
	     * Validation of single rule.
	     *
	     * @param $key
	     * @param $rule
	     * @return void
	     */	
		private function check($key, $rule): void
		{
			foreach($rule as $currentRule)
			{
				$rulesExploded = explode(":", $currentRule);

				$functionName = $rulesExploded[0];

				$functionParameter = (array_key_exists(1, $rulesExploded)) ? $rulesExploded[1] : null;

				$this->$functionName($key, $functionParameter);
			}

		}

	    /**
	     * 'Required' rule validation.
	     *
	     * @param $key
	     * @param $param
	     * @return void
	     */	
		private function required($key, $param = null): void
		{
			if(empty($this->request[$key]))
			{
				$this->errorMessage = $key . " is required!<br>";
				$this->error = true;
			} 
		}

	    /**
	     * 'Exists' rule validation.
	     *
	     * @param $key
	     * @param $table
	     * @return void
	     */	
		private function exists($key, $table): void
		{

			if($this->getResultCount($key, $table) == 0)
			{
				$this->errorMessage = $key . " does not exist in " . $table;
				$this->error = true;
			}
		}

	    /**
	     * 'Unique' rule validation.
	     *
	     * @param $key
	     * @param $table
	     * @return void
	     */	
		private function unique($key, $table): void
		{
			if($this->getResultCount($key, $table) > 0)
			{
				$this->errorMessage = $key . " already exists in table " . $table;
				$this->error = true;
			}
		}

	    /**
	     * 'Length' rule validation.
	     *
	     * @param $key
	     * @param $length
	     * @return void
	     */	
		private function length($key, $length): void
		{
			if(strlen($this->request[$key]) > $length)
			{
				$this->errorMessage = "Maximum of " . $length . " characters are allowed";
				$this->error = true;
			}
		}

	    /**
	     * Gets the current error message if validation failed.
	     *
	     * @return string
	     */	
		public function getMessage(): string
		{
			return $this->errorMessage;
		}


	    /**
	     * Gets the row count to check if records exists.
	     *
	     * @param $key
	     * @param $table
	     * @return int
	     */	
		private function getResultCount($key, $table): int
		{
			$exploded = explode("-", $table);
			$table = $exploded[0];
			$column = $exploded[1];

			$rows = (new QueryBuilder)->table($table)->where([$column, '=', $this->request[$key]])->select()->get();
			return count($rows);
		}
	}


?>
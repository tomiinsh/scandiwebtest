<?php

	/**
     * Extracts class name from its namespace as table name.
     *
     * @param $name
     * @return string
     */
	if(!function_exists('parse_class_name'))
	{
		function parse_class_name($name): string
		{
				$array = explode('\\', $name);
				$className = end($array);
				return strtolower($className);
		}		
	}

	/**
     * Creates CSRF token.
     *
     * @return string
     */
	if(!function_exists('csrf_token'))
	{
		function csrf_token(): string
		{
			return bin2hex(random_bytes(32));
		}		
	}



?>
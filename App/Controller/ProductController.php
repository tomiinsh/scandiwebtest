<?php 
	namespace App\Controller;
	use App\Core\BaseController;
	use App\Model\ProductType;
	use App\Model\Attribute;
	use App\Model\Product;
	use App\Core\Validator\Validator;
	use App\Core\Router;

	class ProductController extends BaseController 
	{
		public function index()
		{
			$this->list();
		}

		public function list()
		{
			$this->view('list', ['products' => Product::getProductList()]);
		}

		public function add()
		{
			if(isset($_POST) && $_POST != null && ($_POST['token'] === $_SESSION['token']))
			{
				$validator = new Validator([
					'sku' => ['required', 'unique:product-sku', 'length:8'],
					'name'=> ['required'],
					'price' => ['required'],
					'type' => ['required', 'exists:producttype-name'],
					'attribute' => ['required']
				]);
			
				if($validator->validate($_POST))
				{
					$product = new Product();
					$product->sku = (string)$_POST['sku'];
					$product->name = (string)$_POST['name'];
					$product->price = (float)$_POST['price'];
					$product->productType = (int)$product->getProductTypeId($_POST['type']);
					$product->prepareProductAttribute($_POST);
					$product->save();
				}

				Router::redirect('product', 'list');
			}

			$this->view('add');
		}

		public function remove()
		{
			if(isset($_POST['skuList']) && $_POST != null)
			{
				$builder = Product::newQueryBuilder()->join('attribute', 'product.sku', '=', 'attribute.sku');

				(count($_POST['skuList']) > 1) ? $builder->whereIn('product.sku', $_POST['skuList']) : $builder->where(['product.sku', '=', $_POST['skuList'][0]]);

				$builder->delete('product', 'attribute');		
			}
		}

		public function checkSku($sku = null)
		{
			if(isset($_POST['sku']))
			{
				$rows = Product::newQueryBuilder()->where(['sku', '=', $_POST['sku']])->select(['sku'])->get();
				echo $response = (count($rows) > 0) ? "false" : "true";				
			}
		}
	}
?>
$( document ).ready(function() {
   
    $("#add-product-form").validate({
        highlight: function(element) { $(element).parent().addClass("form-error"); },
        unhighlight: function(element) { $(element).parent().removeClass("form-error"); },
        errorPlacement: function(error, element) { $(element).parent().after($(error)); },

        // Declaration of input rules for validation.
        rules: {
            sku: {
                required: true,
                minlength: 8, 
                maxlength: 8,

                //To check if a produt with given SKU exists, SKU is sent to /Scanditest/product/checkSku for checking.
                remote: {
                    url: "/Scanditest/product/checkSku",
                    method:"post",
                    data: {
                        url:function() {
                            return $("#sku").val();
                        }
                    }
                }
            },
            name: {required: true, minlength: 1, maxlength: 50},
            price: {required: true, min: 0.01, max: 999999999.99},
            type: {required: true}
            
        },

        // Error messages that are displayed for the user if rules are not met.
        messages: {
            sku: {
                required: "Please enter the SKU!",
                minlength: "SKU must be {0} charactes long!",
                maxlength: "SKU must be {0} charactes long!",
                remote: "This product already exists!"
            },
            name: {
                required: "Please enter the products name!",
                minlength: "Name should be atleast {0} characters long!",
                maxlength: "Name should be less than {0} characters long!"
            },
            price: {
                required: "Please enter the price!",
                min: "Minimum price is $0.01!",
                max: "Maximum price is ${0}"
            },
            type: {
                required: "Please choose the type!"
            }
        }
    });

	//Depending of user choice, forms are loaded from forms.html.
    $("#typeSelector").change(function(){
        var selected = $("#typeSelector").val();

        if (selected == "") $("#add-attributes").fadeOut(200, function(){$(this).html("")});
        else {
            $("#add-attributes").fadeOut(200, function(){

                //Dynamic form loading.
                $("#add-attributes").load("/Scanditest/public/forms.html #"+selected, function(){
                    $(this).fadeIn(200);
                    $("[name^=attribute]").each(function () {

                        // After successful load, a callback function adds additional rules to the new form.
                        $(this).rules("add", {
                            required: true,
                            maxlength: 9,
                            messages: {
                                required: "This attribute is required!"
                            }
                        });
                    });             
                });
            });
        }
    });
});

[@title]
	Product list
[@endtitle]

[@header]
		<div id="header">
			<h1>Product list</h1>
			<div class="button-container">      
					<a href="/Scanditest/product/add" class="btn btn-primary">Add</a>  
					<button id="delete-products-button" class="btn btn-primary list-page-button">Delete</button>
			</div>			
		</div>
[@endheader]  

[@content]
	<div class="card-group">
		<?php if(!empty($data['products'])): ?>
			<?php foreach($data['products'] as $product): ?>
			<div class="col-md-3">
				<div class="card">
					<div class="card-body">
						<div class="form-check">
								<input class="form-check-input" type="checkbox" name="product-checkbox" value="<?php echo $product['sku']; ?>" id="flexCheckDefault">
						</div>
						<h5 class="card-title"><?php echo $product['name']; ?></h5>
						<p class="card-text">
							<?php 
								foreach($product['attribute'] as $attribute)
								{
									echo ucfirst($attribute['name']) . ": " . $attribute['value'] . $attribute['uom'] . " ";
								}
							?>
						</p>
						<p class="card-text"><small class="text-muted"><?php echo $product['price']; ?> $</small></p>
					</div>
				</div>            
			</div>
			<?php endforeach; ?> 
		<?php else: ?>
			<div><h2>There are no products to show!</h2></div>
		<?php endif; ?>            
	</div>
[@endcontent]

[@script]
	<script src="/Scanditest/public/javascript/delete-products.js"></script>
[@endscript]
<?php
	namespace App\Model\Products;
	use App\Model\Products\Interfaces\BaseProductInterface;

	class Book implements BaseProductInterface
	{
		public $attributes = ['weight'];
		public $uom = 'KG';

		public function buildAttributeValue($attributes)
		{
			return $attributes['weight'];
		}	
	}
?>
<?php
    namespace App\Core\QueryBuilder\Statement;
    use App\Core\QueryBuilder\QueryBuilder;
    use App\Core\QueryBuilder\Statement\Interfaces\Statement;

    class Delete implements Statement 
    {
        public static function buildQuery(QueryBuilder $props): string 
        {
            $where = $props->whereClause === '' ? '' : ' WHERE ' . $props->whereClause;
            $join = $props->joinClause === [] ? '' : implode(" ", $props->joinClause);

            $query = "DELETE " . $props->columns . " FROM " . $props->table . $join . $where;
 
            return $query;
        }
    }
?>
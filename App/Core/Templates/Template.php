<?php
	namespace App\Core\Templates;

	class Template
	{
        /**
         * The name of view.
         *
         * @var string $view
         */     		
		public $view;

        /**
         * The data array.
         *
         * @var array $data
         */ 		
		public $data;

        /**
         * The base template file name.
         *
         * @var string $baseTemplate
         */ 		
		private $baseTemplate = 'BaseTemplate.php';

        /**
         * The list of fields that needs to be replaced in the template.
         *
         * @var arrat $fields
         */ 			
		private $fields = array (
			'title',
			'header',
			'content',
			'footer',
			'script'
		);

        /**
         * Create new Template class instance.
         *
         * @param  string $view
         * @return void
         */
		public function __construct($view)
		{
			$this->view = $view;
		}

        /**
         * Renders view from the template.
         *
         * @param  array $datta
         * @return void
         */
		public function render($data): void
		{
			$template = file_get_contents(__DIR__ . '\\' . $this->baseTemplate);
			$view = $this->getRenderedContent(__DIR__ . '/../../View/' . $this->view . '.php', $data);

			foreach($this->fields as $field)
			{
				$content = $this->getSectionContents($view, "[@$field]", "[@end$field]");
				$template = str_replace("[@$field]", $content, $template);
			}

			echo $template;
		}

        /**
         * Returns string portion that is in between two input strings.
         *
         * @param string $string
         * @param string $start
         * @param string $end
         * @return string
         */
		private function getSectionContents($string, $start, $end): string
		{
		    $string = ' ' . $string;
		    $ini = strpos($string, $start);
		    if ($ini == 0) return '';
		    $ini += strlen($start);
		    $len = strpos($string, $end, $ini) - $ini;
		    return substr($string, $ini, $len);			
		}

        /**
         * Renders php code and returns html code as string.
         *
         * @param string $view
         * @param array $data
         * @return string
         */
		private function getRenderedContent($view, $data): string
		{
		    ob_start();

		    include_once($view);
		    $renderedContent = ob_get_contents();

		    ob_end_clean();
		    return $renderedContent;
		}

	}

?>
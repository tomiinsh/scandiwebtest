<?php 
	namespace App\Model;
	use App\Core\BaseModel;

	class Attribute extends BaseModel 
	{
		public string $sku;
		public int $attributeType;
		public string $value;

		protected $fillable = ['sku', 'attributetype', 'value'];
	}
?>
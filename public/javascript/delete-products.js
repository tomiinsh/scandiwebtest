$( document ).ready(function() {

	var url = "/Scanditest/product/remove";

	$("#delete-products-button").on('click', function(){
		var skuList = [];

		$("input:checkbox[name=product-checkbox]:checked").each(function(){
			skuList.push($(this).val());
		});

		console.log(skuList);

		$.ajax({
			type:"POST",
			url: url,
			data: {
				skuList: skuList
			}
		});

		location.reload();
	});
});
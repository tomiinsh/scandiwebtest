<?php
    namespace App\Core\QueryBuilder\Statement;
    use App\Core\QueryBuilder\Statement\Interfaces\Statement;
    use App\Core\QueryBuilder\QueryBuilder;

    class Insert implements Statement 
    {
        public static function buildQuery(QueryBuilder $props): string 
        {
            $query = "INSERT INTO " . $props->table . "(" . $props->columns . ") VALUES (" . $props->placeholders . ")";
            return $query;
        }
    }
?>
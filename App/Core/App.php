<?php
	namespace App\Core;
	use App\Core\Router;

	class App 
	{
	    /**
	     * Create App instance.
	     *
	     * @return void
	     */	
		public function __construct()
		{	
			(new Router)->set();
		}
	}

?>
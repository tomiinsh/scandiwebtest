<?php
	namespace App\Model;
	use App\Core\BaseModel;
	use App\Model\AttributeType;
	use App\Model\Attribute;

	class Product extends BaseModel 
	{

		public string $sku;
		public string $name;
		public float $price;
		public int $productType;

		protected $fillable = ['sku', 'name', 'price', 'producttype'];

		public function prepareProductAttribute(array $data)
		{

			$className = "App\Model\Products\\" . $data['type'];
			$productType = new $className;

			print_r($data['attribute']);

			if ($this->checkAttributeCount($productType->attributes, $data['attribute'])); 
			{
				$this->setAttributes($data['attribute'], $productType);	
			}
		}

		private function setAttributes($attributes, $productType)
		{
			foreach($attributes as $attributeKey => $attributeValue)
			{
				$attribute = new Attribute();
				$attribute->sku = $this->sku;
				$attribute->attributeType = $this->getProductTypeAttributeId($attributeKey, $productType->uom);
				$attribute->value = $productType->buildAttributeValue($attributeValue);
				$attribute->save();
			}
		}

		private function checkAttributeCount($productTypeAttributes, $postDataAttributes): bool
		{
			return (array_count_values($productTypeAttributes) == array_count_values(array_keys($postDataAttributes)));
		}

		private function getProductTypeAttributeId($name, $uom)
		{
			return AttributeType::newQueryBuilder()->where(['name', '=', $name], ['uom', '=', $uom])
												   ->select(['id'])->get()[0]['id'];
		}

		public function delete($sku)
		{
			Product::newQueryBuilder()->join('attribute', 'product.sku', '=', 'attribute.sku')
										->where(['product.sku', '=', $sku])
										->delete('product', 'attribute');		
		}

		public static function getProductList()
		{
			$list = Product::newQueryBuilder()->select(['sku', 'name', 'price'])->get();

			foreach($list as &$listItem) $listItem['attribute'] = self::getProductAttributes($listItem['sku']);
			
			return $list;
		}


		public static function getProductAttributes($sku)
		{
			return Attribute::newQueryBuilder()->join('attributetype', 'attribute.attributetype', '=', 'attributetype.id')
														->where(['attribute.sku', '=', $sku])
														->select(['attribute.value', 'attributetype.name', 'attributetype.uom'])
														->get();
		}

		public function getProductTypeId($type)
		{
			return ProductType::newQueryBuilder()->where(['name','=', $type])->select(['id'])->get()[0]['id'];
		}
	}

?>
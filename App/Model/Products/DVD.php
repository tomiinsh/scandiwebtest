<?php
	namespace App\Model\Products;
	use App\Model\Products\Interfaces\BaseProductInterface;
	
	class DVD implements BaseProductInterface
	{
		public $attributes = ["size"];
		public $uom = "GB";

		public function buildAttributeValue($attributes)
		{
			return $attributes['size'];
		}
	}
?>
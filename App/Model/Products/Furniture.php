<?php
	namespace App\Model\Products;
	use App\Model\Products\Interfaces\BaseProductInterface;
	
	class Furniture implements BaseProductInterface
	{
		public $attributes = ["dimensions"];
		public $uom = '';

		public function buildAttributeValue($attributes)
		{
			$attributeValue = $attributes['height'] . "x" . $attributes['width'] . "x" . $attributes['length'];
			return $attributeValue;
		}
	}	

?>
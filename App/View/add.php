[@title]
Add New Products
[@endtitle]
[@header]
    <div id="header">
        <h1>Add products</h1>
        <div class="button-container">
                <button id="delete-products-button" form="add-product-form" class="btn btn-primary">Save</button>                 
                <a href="/Scanditest/product/list" class="btn btn-primary">Cancel</a>  
        </div>
        <hr>        
    </div> 
[@endheader]

[@content]
    <div class="col-md-4 offset-md-4">
        <form action="" method="post" enctype="multipart/form-data" id="add-product-form">

            <div class="input-group mb-3">
                <input type="text" class="form-control" id="sku" name="sku" placeholder="SKU" aria-label="SKU" aria-describedby="basic-addon1">
            </div>

            <div class="form-group mb-3">
                <input type="text" class="form-control" id="name" name="name" placeholder="Name" aria-lael="Name" aria-descibeby="basic-addon1">
            </div>

            <div class="input-group mb-3">
                <input type="number" class="form-control" step="0.01" id="price" name="price" placeholder="Price" aria-label="Price">
                <div class="input-group-append">
                    <span class="input-group-text">$</span>
                </div>
            </div>

            <div class="input-group mb-3">
                <select class="form-select" id="typeSelector" name="type">
                    <option value="">Choose...</option>
                    <option value="DVD">DVD</option>
                    <option value="Book">Book</option>
                    <option value="Furniture">Furniture</option>
                </select>            
            </div>
            <div id="add-attributes"></div>
            <input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>">
        </form>     
    </div>
[@endcontent]

[@script]
    <script src="/Scanditest/public/javascript/add-product-validation.js"></script>
[@endscript]
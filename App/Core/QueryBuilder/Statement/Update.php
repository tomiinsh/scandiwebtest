<?php
    namespace App\Core\QueryBuilder\Statement;

    use App\Core\QueryBuilder\Statement\Interfaces\Statement;
    use App\Core\QueryBuilder\QueryBuilder;

    class Update implements Statement 
    {
        public static function buildQuery(QueryBuilder $props): string 
        {
            $where = $props->whereClause === '' ? '' : ' WHERE ' . $props->whereClause;
            $query = "UPDATE " . $props->table . " SET " . $props->updateClause . $where;
            return $query;
        }
    }
?>
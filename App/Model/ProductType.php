<?php
	namespace App\Model;
	use App\Core\BaseModel;

	class ProductType extends BaseModel {
		public int $id;
		public string $name;
		
		protected $table = "producttype";		
		protected $fillable = ['name'];
	}
?>
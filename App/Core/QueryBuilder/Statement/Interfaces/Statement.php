<?php
    namespace App\Core\QueryBuilder\Statement\Interfaces;
    use App\Core\QueryBuilder\QueryBuilder;

    interface Statement 
    {
    	/**
         * Builds SQL query string.
         *
         * @param  App\Core\QueryBuilder\QueryBuilder $props
         * @return string
         */
        public static function buildQuery(QueryBuilder $props): string;
    }
?>
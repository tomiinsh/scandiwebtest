<?php
	namespace App\Core;
	use App\Core\BaseController;
	
	class Router extends BaseController 
	{

	    /**
	     * The controller name.
	     *
	     * @var string
	     */		
		private $controller = DEFAULT_CONTROLLER;

	    /**
	     * The method name.
	     *
	     * @var string
	     */			
		private $method = DEFAULT_CONTROLLER_METHOD;

	    /**
	     * The parameter array.
	     *
	     * @var array
	     */			
		private $params = [];		

		/**
	     * Routing set-up.
	     *
	     * @return array
	     */
		public function set(): void
		{
			$url = $this->parseURL();
			$this->setController($url);

			if($url !== null)
			{
				$this->setMethod($url);
				$this->setParameters($url);					
			}
			
			$this->callControllerMethod();
		}

		/**
	     * Converts URL into array of controller-method-params
	     *
	     * @return mixed $url
	     */
		private function parseURL()
		{
			$url = (isset($_GET['url'])) ? explode('/', filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL)) : null;

			return $url;
		}

		/**
	     * Sets controller based on URL.
	     *
	     * @param $url
	     * @return void
	     */
		private function setController(&$url): void
		{
			if(isset($url))
			{
				if(class_exists('App\Controller\\' . $url[0] . 'Controller')) 
				{
					$this->controller = $url[0];
					unset($url[0]);
				}				
			}
	
			$className = 'App\Controller\\' . $this->controller . 'Controller';
			$this->controller = new $className;		
		}

		/**
	     * Sets controller method.
	     *
	     * @param $url
	     * @return void
	     */
		private function setMethod(&$url): void
		{
			if(isset($url[1]) && method_exists($this->controller, $url[1]) && is_callable(array($this->controller, $this->method))) 
			{
				$this->method = $url[1];
				unset($url[1]);
			}			
		}

		/**
	     * Sets any parameters that are given in the URL.
	     *
	     * @param $url
	     * @return void
	     */
		private function setParameters($url): void
		{
			$this->params = $url ? array_values($url) : [];
		}

		/**
	     * Calls controller method.
	     *
	     * @return void
	     */
		private function callControllerMethod(): void
		{
			call_user_func_array([$this->controller, $this->method], $this->params);
		}

		/**
	     * Redirects user to a view.
	     *
	     * @return void
	     */
		public static function redirect($controller, $view): void
		{
			header('Location: /Scanditest/' . $controller . '/'. $view);
		}
	}

?>
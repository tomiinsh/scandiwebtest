<?php
	namespace App\Model\Products\Interfaces;

	/**
	 * Creates attribute value from given array.
	 *
	 * @param array $attribute
	 * @return string
	 */
	interface BaseProductInterface
	{
		public function buildAttributeValue($attribute);
	}
?>
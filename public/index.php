<?php
	require_once '../App/vendor/autoload.php';
	use App\Core\App;
	session_start();

	if(!isset($_SESSION['token'])) $_SESSION['token'] = csrf_token();
		
	$app = new App();
?>
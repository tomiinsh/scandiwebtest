<?php
    namespace App\Core\QueryBuilder\Statement;
    use App\Core\QueryBuilder\Statement\Interfaces\Statement;
    use App\Core\QueryBuilder\QueryBuilder;

    class Select implements Statement 
    {
        public static function buildQuery(QueryBuilder $props): string 
        {
            $where = $props->whereClause === '' ? '' : ' WHERE ' . $props->whereClause;

            $orderBy = $props->orderBy === [] ? '' : ' ORDER BY ' . implode(', ', $props->orderBy);
            $limit = $props->limit === null ? '' : ' LIMIT ' . $props->limit;
            $offset = $props->offset === null ? '' : ' OFFSET ' . $props->offset;
            $join = $props->joinClause === [] ? '' : implode(" ", $props->joinClause);

            $query = "SELECT " . $props->columns . " FROM " . $props->table . $join . $where . $orderBy . $limit . $offset;
            
            return $query;
        }
    }
?>
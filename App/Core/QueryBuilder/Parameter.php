<?php
	namespace App\Core\QueryBuilder;


	class Parameter
	{
    	/**
         * The name of the parameter
         *
         * @var string $name
         */		
		public string $name;

    	/**
         * Operator for the parameter - value pair.
         *
         * @var string $operator
         */			
		public string $operator;

    	/**
         * The parameters value.
         *
         * @var mixed $value
         */			
		public $value;

    	/**
         * Placeholder for the parameter that will be used to build SQL query.
         *
         * @var string $placeholder
         */			
		public string $placeholder;

    	/**
         * Create new Parameter class instance.
         *
         * @param string $name
         * @param string $operator
         * @param mixed $value
         * @param int $count
         * @return void
         */
		public function __construct($name, $operator = '=', $value, $count)
		{
			$this->name = $name;
			$this->operator = $operator;
			$this->value = $value;
			$this->placeholder = ":param" . $count;
		}
	}

?>
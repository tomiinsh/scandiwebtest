<?php
    namespace App\Core\QueryBuilder;
    use App\Core\Database\DB;
    use App\Core\QueryBuilder\Parameter;

    class QueryBuilder {

        /**
         * The list of table columns.
         *
         * @var array $columns
         */        
        public $columns;

        /**
         * The list column names that is used for ORDER BY clause.
         *
         * @var array $orderBy
         */         
        public $orderBy = [];

        /**
         * Limit integer for LIMIT clause.
         *
         * @var int $limit
         */         
        public $limit;

        /**
         * Offset integer for OFFSET clause.
         *
         * @var int $offset
         */             
        public $offset;

        /**
         * The name of the table where query will be executed.
         *
         * @var string $table
         */           
        public $table;

        /**
         * The list of placeholders that will be used in the query.
         *
         * @var mixed $placeholders
         */          
        public $placeholders;

        /**
         * The list of query parameters that will be used in the query.
         *
         * @var array $queryParams
         */          
        public $queryParameters = [];

        /**
         * Where clause for the query.
         *
         * @var string $whereClause
         */          
        public $whereClause = "";

        /**
         * The query that will be executed in the database.
         *
         * @var string $query
         */          
        public $query = "";

        /**
         * Update clause that will be used in the query.
         *
         * @var string $updateClause
         */          
        public $updateClause;

        /**
         * Lis of JOIN clauses that will be used in the query.
         *
         * @var array $joinClause
         */            
        public $joinClause = [];

        /**
         * List of name - operator - value parameters for the query.
         *
         * @var array $parameters
         */ 
        private $parameters = [];

        /**
         * The name of the query action.
         *
         * @var string $action
         */         
        private $action;

        /**
         * The result set after successful query.
         *
         * @var array $results
         */         
        private $results;

        /**
         * The list of query actions mapped to their classes.
         *
         * @var array $statementMap
         */         
        private $statementMap = array (
            'SELECT' => 'Select',
            'INSERT' => 'Insert',
            'UPDATE' => 'Update',
            'DELETE' => 'Delete'
        );

        /**
         * Create new QueryBuilder instance.
         *
         * @param  string $table|null
         * @return void
         */
        public function __construct($table = null) 
        {
            $this->table = $table;
        }

        /**
         * Set table name for current query.
         *
         * @param  string $name
         * @return $this
         */
        public function table(string $name): self 
        {
            $this->table = $name;
            return $this;
        }

        /**
         * Execute SELECT statement.
         *
         * @param  array $fields
         * @return $this
         */
        public function select(array $columns = ['*']): self 
        {
            $this->setTableColumnNames($columns)->setAction('SELECT')->setQuery($this->buildQueryString())->executeQuery();
            return $this;
        }

        /**
         * Execute INSERT statement.
         *
         * @param  array $data
         * @return $this
         */
        public function insert(array ...$data): self 
        {
            $this->setAction('INSERT');
            foreach($data as $column) 
            {
                $this->queryParameters = $placeholders = [];
                $this->columns = implode(", ", array_keys($column));
                $this->queryParameters = $column;
                $this->setPlaceholders($column)->setQuery($this->buildQueryString())->executeQuery();
            }

            return $this;
        }

        /**
         * Execite update statement.
         *
         * @param  array $data
         * @return $this
         */
        public function update(array $data): self 
        {
            $this->setAction('UPDATE');
            
            $this->setQueryParameters($data)->setUpdateClause($data)->setQuery($this->buildQueryString())->executeQuery();

            return $this;
        }

        /**
         * Sets update clause string.
         *
         * @return this
         */
        private function setUpdateClause(): self
        {
            $this->updateClause = "(" . implode(", ", $this->buildClause()) . ")";
            return $this;
        }

        /**
         * Begin to create delete statement.
         *
         * @return void
         */
        public function delete(string ...$columns): self
        {
            $this->setAction('DELETE')->setTableColumnNames($columns)->setQuery($this->buildQueryString())->executeQuery();
            return $this;
        }

        /**
         * Begin to create basic where clause.
         *
         * @param array ...$data
         * @return $this
         */  
        public function where(array ...$data): self
        {

            if ($this->checkWhereClauseData($data)) $this->setQueryParameters($data)->setWhereClause();
            return $this;           
        }
        
        /**
         * Begin WHERE IN clause.
         *
         * @param string $field
         * @param array $list
         * @return $this
         */ 
        public function whereIn($field, $list)
        {
            $bindings = implode(',', array_fill(0, count($list), '?'));
            $this->whereClause = "(" . $field . " IN (" . $bindings . "))";
            $this->queryParameters = $list;
            
            return $this;         
        }

        /**
         * Check for empty where clause.
         *
         * @param $fields
         * @return bool
         */
        public function isWhereClauseEmpty($fields): bool
        {
           return (sizeof($fields) != 0);
        }

        /**
         * Check if 3 arguments for each where clause field is provided.
         *
         * @param $fields
         * @return bool
         */
        public function isWhereClauseFieldCountCorrect($fields): bool
        {
            foreach($fields as $field) if(sizeof($field) != 3) return false;
            return true;              
        }

        /**
         * Where clause input validation.
         *
         * @param $fields
         * @return bool
         */
        private function checkWhereClauseData($fields): bool
        {
            return ($this->isWhereClauseEmpty($fields) && $this->isWhereClauseFieldCountCorrect($fields));
        }

        /**
         * Sets where clause string.
         *
         * @return void
         */        
        private function setWhereClause(): void
        {
            $this->whereClause = "(" . implode(" AND ", $this->buildClause()) . ")";        
        }

        /**
         * Creates parameter - value - pair array for the query.
         *
         * @return array
         */   
        private function buildClause(): array
        {
            $pairs = [];

            foreach($this->parameters as $parameter)
            {
                $pair = $parameter->name . " " . $parameter->operator . " " . $parameter->placeholder;
                array_push($pairs, $pair);
            }

            $this->parameters = [];            

            return $pairs;
        }

        /**
         * Set parameters for where and update caluses.
         *
         * @param $data
         * @return $this
         */    
        public function setQueryParameters($data): self
        {
            foreach($data as $key => $value)
            {
                $count = count($this->queryParameters) + 1;
                
                //if called from where clause
                if(is_countable($value))
                {
                    $param = new Parameter($value[0], $value[1], $value[2], $count);              
                }
                //if called from update clause
                else
                {
                    $param = new Parameter($key, '=', $value, $count);            
                }

                array_push($this->parameters, $param);
                $this->queryParameters[$param->placeholder] = $param->value;
            }
            return $this;
        }

        /**
         * Adds JOIN clause to the join list.
         *
         * @return $this
         */   
        public function join($table, $column1, $operator, $column2): self
        {
            array_push($this->joinClause, " JOIN " . $table . " ON " . $column1 . $operator . $column2);
            return $this;
        }

        /**
         * Add order by clause.
         *
         * @param int ...$orderBy
         * @return $this
         */ 
        public function orderBy(string ...$orderBy): self 
        {
            foreach ($orderBy as $field) $this->orderBy[] = $field;
            return $this;
        }

        /**
         * Add limit clause.
         *
         * @param int $limit
         * @return $this
         */ 
        public function limit(int $limit): self 
        {
            $this->limit = $limit;
            return $this;
        }

        /**
         * Add offset clause.
         *
         * @param int $offset
         * @return $this
         */ 
        public function offset(int $offset): self 
        {
            $this->offset = $offset;
            return $this;
        }

        /**
         * Build final SQL query string.
         *
         * @return string
         */  
        public function buildQueryString(): string 
        {
            $classname = __NAMESPACE__ . '\\Statement\\' . $this->statementMap[$this->action];
            return $classname::buildQuery($this);      
        }

        /**
         * Fetch DB data.
         *
         * @return array
         */          
        public function get(): array 
        {
            return $this->results->fetchAll();
        }

        /**
         * Executes SQL query.
         *
         * @return $this
         */    
        private function executeQuery(): self
        {
            $this->results = DB::execute($this->query, $this->queryParameters);
            return $this;
        }

        /**
         * Build placeholder string.
         *
         * @return this
         */        
        private function setPlaceholders($data): self
        {
            $this->placeholders = implode(", ", array_map(function($a, $b){ return ":" . $b; }, $data, array_keys($data)));
            return $this;
        }

        /**
         * Set statement action.
         *
         * @param $action
         * @return $this
         */ 
        private function setAction($action): self
        {
            $this->action = $action;
            return $this;
        }

        /**
         * Set query string.
         *
         * @param $query
         * @return $this
         */ 
        private function setQuery($query): self
        {
            $this->query = $query;
            return $this;
        }

        /**
         * Set table column names.
         *
         * @param $fields
         * @return $this
         */ 
        private function setTableColumnNames($columns): self
        {
            $this->columns = $this->buildColumnNamesString($columns);
            return $this;
        }

        /**
         * Create table column name string.
         *
         * @param $fields
         * @return $string
         */ 
        private function buildColumnNamesString($columns): string
        {
            return implode(", ", $columns);
        }
    }
?>
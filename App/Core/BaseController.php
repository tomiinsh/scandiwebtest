<?php
	namespace App\Core;
	use App\Core\BaseModel;
	use App\Core\Templates\Template;
	
	abstract class BaseController 
	{
	    /**
	     * Loads the view.
	     *
	     * @param $view
	     * @param $data
	     * @return void
	     */			
		public function view($view, $data = null): void
		{
			$template = new Template($view);
			$template->render($data);
		}
	}
?>
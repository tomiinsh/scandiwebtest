<?php
	namespace App\Model;
	use App\Core\BaseModel;

	class AttributeType extends BaseModel 
	{	
		public int $id;
		public string $name;
		public string $uom;

		protected $table = "attributetype";	
		protected $fillable = ['name', 'uom'];
	}
?>